# ambiorix_test

This project is intended to test Ambiorix functionality.
To use, see the gitlab pipelines.
It can also be manually tested on your own machine: just run `make`

NOTE: WIP

# What it does (and is intended to do)

This project will allow you to:
- clone all relevant ambiorix repos (including multiple backends)
- build them
- run tests using the various different ambiorix backends
- have this be automated in gitlab CI

The `Makefile` will do the first 2 steps
The `run_test.py` script will run the tests
The `.gitlab-ci.yaml` file will run it in docker as part of the gitlab CI


