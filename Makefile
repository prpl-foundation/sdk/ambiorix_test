AMBIORIX_LIBS := libamxc libamxp libamxd libamxb libamxj libamxo libamxt libamxm
AMBIORIX_BACKENDS := amxb_dbus amxb_ubus amxb_rbus 
AMBIORIX_APPS := amx-cli

MAKEFILEDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
STAGINGDIR := $(MAKEFILEDIR)build
DEST := $(STAGINGDIR)
PREFIX :=
OUT_LIBDIR := $(STAGINGDIR)/lib
OUT_INCDIR := $(STAGINGDIR)/include
OUT_BINDIR := $(STAGINGDIR)/bin


ambiorix: $(AMBIORIX_LIBS) $(AMBIORIX_BACKENDS) $(AMBIORIX_APPS)

$(STAGINGDIR):
	mkdir -p $@
	-ln -s $@ $@/usr

$(OUT_LIBDIR) $(OUT_INCDIR) $(OUT_BINDIR): $(STAGINGDIR)
	mkdir -p $@
	-ln -s $@ $@/$(shell gcc -dumpmachine)


$(AMBIORIX_LIBS): $(OUT_LIBDIR) $(OUT_INCDIR) $(OUT_BINDIR)
	[ -d $@ ] || git clone git@gitlab.com:prpl-foundation/components/ambiorix/libraries/$@
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ 
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ install

$(AMBIORIX_BACKENDS): $(AMBIORIX_LIBS) $(OUT_LIBDIR) $(OUT_BINDIR) $(OUT_INCDIR)
	[ -d $@ ] || git clone git@gitlab.com:prpl-foundation/components/ambiorix/modules/amxb_backends/$@
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ 
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ install

$(AMBIORIX_APPS): $(AMBIORIX_LIBS) $(OUT_BINDIR) ALWAYS
	[ -d $@ ] || git clone git@gitlab.com:prpl-foundation/components/ambiorix/applications/$@
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ 
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -EDEST=$(DEST) -C $@ install


libamxp: libamxc

libamxd: libamxc libamxp

libamxo: libamxc libamxp libamxd 

libamxb: libamxc libamxp libamxd uriparser

libamxj: libamxc

libamxt: libamxc libamxp

libamxm: libamxc ALWAYS

amxb_dbus: dbus

amxb_ubus: libubox ubus

amx-cli: libamxj libamxo libamxt libamxm

libubox: $(OUT_INCDIR) $(OUT_LIBDIR)
	[ -d $@ ] || git clone https://git.openwrt.org/project/libubox.git
	cd $@ && \
		cmake . -DCMAKE_INSTALL_PREFIX=$(STAGINGDIR) -DBUILD_LUA=OFF -DBUILD_EXAMPLES=OFF && \
		make && make install
	cd $(MAKEFILEDIR)


ubus: $(OUT_BINDIR) libubox
	[ -d $@ ] || git clone https://git.openwrt.org/project/ubus.git
	cd $@ && \
		cmake . -DCMAKE_INSTALL_PREFIX=$(STAGINGDIR) -DBUILD_LUA=OFF -DBUILD_EXAMPLES=OFF && \
		make && make install
	cd $(MAKEFILEDIR)


libexpat: $(OUT_INCDIR) $(OUT_BINDIR) $(OUT_LIBDIR)
	[ -d $@ ] || git clone https://github.com/libexpat/libexpat.git
	cd $@/expat && \
		cmake . -DEXPAT_BUILD_TESTS=OFF -DEXPAT_BUILD_EXAMPLES=OFF -DEXPAT_BUILD_DOCS=OFF\
		-DCMAKE_INSTALL_PREFIX=$(STAGINGDIR) && \
		make && make install
	cd $(MAKEFILEDIR)

dbus: $(OUT_INCDIR) $(OUT_BINDIR) $(OUT_LIBDIR) libexpat
	[ -d $@ ] || git clone git@gitlab.freedesktop.org:dbus/dbus.git
	cd $@ && \
		cmake . -DDBUS_BUILD_TESTS=OFF -DDBUS_ENABLE_DOXYGEN_DOCS=OFF \
			-DDBUS_ENABLE_SYSTEMD=OFF -DDBUS_ENABLE_QT_HELP=OFF \
			-DDBUS_ENABLE_XML_DOCS=OFF -DDBUS_INSTALL_SYSTEM_LIBS=ON \
			-DCMAKE_INSTALL_PREFIX=$(STAGINGDIR) && \
		make && make install
	cd $(MAKEFILEDIR)
	-ln -s $(OUT_INCDIR)/dbus-1.0/dbus $(OUT_INCDIR)/dbus
	-ln -s $(OUT_LIBDIR)/dbus-1.0/include/dbus/dbus-arch-deps.h $(OUT_INCDIR)/dbus/dbus-arch-deps.h

libsahtrace: $(OUT_INCDIR) $(OUT_BINDIR) $(OUT_LIBDIR)
	[ -d $@ ] || git clone git@gitlab.com:prpl-foundation/components/core/libraries/libsahtrace
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -ED=$(DEST) -C $@ 
	$(MAKE) -ESTAGINGDIR=$(STAGINGDIR) -ED=$(DEST) -C $@ install


uriparser: $(OUT_INCDIR) $(OUT_BINDIR) $(OUT_LIBDIR)
	[ -d $@ ] || git clone https://github.com/uriparser/uriparser.git
	cd $@ && \
		cmake . -DURIPARSER_BUILD_TESTS=OFF -DURIPARSER_BUILD_DOCS=OFF \
			-DCMAKE_INSTALL_PREFIX=$(STAGINGDIR) \
			-DCMAKE_INSTALL_LIBDIR=$(OUT_LIBDIR) \
			-DCMAKE_INSTALL_INCLUDEDIR=$(OUT_INCDIR) \
			-DCMAKE_INSTALL_BINDIR=$(OUT_BINDIR) && \
		make && make install
	cd $(MAKEFILEDIR)

ALWAYS:

clean:
	rm -rf $(STAGINGDIR)
	find . -name *.o -delete

nuke:
	rm -rf $(AMBIORIX_LIBS) $(AMBIORIX_BACKENDS) $(STAGINGDIR) uriparser libexpat dbus libsahtrace

.PHONY: clean nuke ALWAYS

